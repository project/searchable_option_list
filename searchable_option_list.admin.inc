<?php
/**
 * @file
 * SOL administration pages.
 */

/**
 * Returns with the general configuration form.
 */
function searchable_option_list_admin_settings_form($form, &$form_state) {

  // Return without settings, if library is not installed
  $library_path = libraries_get_path('searchable-option-list');
  if (!$library_path) {
    drupal_set_message(t('The library could not be detected. You need to download the !sol and extract the entire contents of the archive into the %path directory on your server.',
      array(
        '!sol' => t('Searchable Option List libary'),
        '%path' => 'sites/all/libraries',
      )
    ), 'error');
    return;
  }

  $form['searchable_option_list_show_select_all'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show "Select all"'),
    '#default_value' => variable_get('searchable_option_list_show_select_all', FALSE),
    '#description' => t('If enabled, SOL will render two links allowing the user to (de-)select all options at once'),
  );
  $form['searchable_option_list_show_selection_below_list'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show selection below list'),
    '#default_value' => variable_get('searchable_option_list_show_selection_below_list', FALSE),
    '#description' => t('If enabled, the selected items will be displayed below the input element, instead of above.'),
  );
  $form['searchable_option_list_allow_null_selection'] = array(
    '#type' => 'checkbox',
    '#title' => t('Allow null selection'),
    '#default_value' => variable_get('searchable_option_list_allow_null_selection', FALSE),
    '#description' => t('Only relevant for single selection SOL (radio button mode): in standard html there is no possibility to deselect an already checked radio button again. If enabled, SOL will allow the user to do that'),
  );
  $form['searchable_option_list_max_height'] = array(
    '#type' => 'textfield',
    '#title' => t('Max. height'),
    '#field_suffix' => 'px',
    '#size' => 3,
    '#default_value' => variable_get('searchable_option_list_max_height', NULL),
    '#description' => t('You may use this option to set the height of the option popup. If you don\'t set this option, the list will be as long as it needs to be'),
    '#element_validate' => array('element_validate_integer_positive'),
  );

  return system_settings_form($form);
}