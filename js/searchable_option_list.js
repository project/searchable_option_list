/**
 * @file
 * The Searchable Option List Drupal Behavior to apply the SOL jQuery plugin to
 * select lists.
 */

(function ($) {
  Drupal.behaviors.searchableOptionList = {
    attach: function (context, settings) {
      // get SOL settings, if any
      settings.searchableOptionList = settings.searchableOptionList || Drupal.settings.searchableOptionList || {};
      // Process searchable options list elements, where SOL is enabled
      $('select.sol-enabled', context).once('sol-enabled', function () {
        var $select = $(this);
        if (!$select.prop('multiple')) {
          settings.searchableOptionList.showSelectAll = false;
        }
        $select.searchableOptionList(settings.searchableOptionList);
      });
    }
  };
})(jQuery);