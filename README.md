# Searchable Option List (SOL)
Integration of [Searchable Option List (SOL)](https://github.com/pbauerochse/searchable-option-list) library with Drupal to make your <select> elements more user-friendly.

## How does it work?
The module extends a renderable array of a select widget with a `#searchable_option_list` boolean flag.

Once it is set to true, pre-render callback adds additional CSS classes _sol_ and _sol-enabled_ to the select element.

The JS will then apply SOL on all select lists with _sol-enabled_ CSS class

## Requirements
- jQuery v1.7 or higher. jQuery will be automatically updated with _jquery_udpate_ module, which is a dependency
- libraries module, which will be automatically installed as a dependecy
- SOL library at latest version

## Installation
1. Download and enable the module via admin panel or using drush with `drush en searchable_option_list`
2. Install the SOL library by downloading manually into your library path or using drush with `drush sol-plugin`

## Usage
The widget can be enabled on any field of type _term reference_ and _entity reference_ in admin panel. Just move to the field settings of your entity and change the widget

### Progammatically
Use `hook_form_FORM_ID_alter()` hook for the form, where you want it to be applied:
```php
if (isset($form['your-select-field']) && $form['your-select-field']['#type'] == 'select') {
  $form['your-select-field']['#searchable_option_list'] = TRUE;
}
```

## Known issues
This module is an alternative to jQuery Chosen. Usage of both modules together was not tested and may result in conflicts. Use with caution.