<?php

/**
 * @file
 * Contains all router functions for the SOL module.
 */

/**
 * Implements hook_menu().
 */
function searchable_option_list_menu() {
  $items = array();

  $items['admin/config/user-interface/searchable-option-list'] = array(
    'title' => 'Searchable Option List (SOL) Settings',
    'description' => 'Settings for Searchable Option List (SOL) widget',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('searchable_option_list_admin_settings_form'),
    'access arguments' => array('administer site configuration'),
    'file' => 'searchable_option_list.admin.inc',
  );

  return $items;
}
