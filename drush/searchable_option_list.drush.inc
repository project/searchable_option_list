<?php

/**
 * @file
 * Drush integration for Searchable Option List.
 */

/**
 * The SOL plugin URI
 */
define('SOL_DOWNLOAD_URI', 'https://github.com/pbauerochse/searchable-option-list/archive/master.zip');

/**
 * Implements hook_drush_command().
 */
function searchable_option_list_drush_command() {
  $items = array();

  // The key in the $items array is the name of the command.
  $items['searchable-option-list-plugin'] = array(
    'callback' => 'drush_searchable_option_list_plugin',
    'description' => dt('Download and install the SOL plugin.'),
    // No bootstrap.
    'bootstrap' => DRUSH_BOOTSTRAP_DRUSH,
    'arguments' => array(
      'path' => dt('Optional. A path where to install the SOL plugin. If omitted Drush will use the default location.'),
    ),
    'aliases' => array('sol-plugin', 'solplugin'),
  );

  return $items;
}

/**
 * Implements hook_drush_help().
 */
function searchable_option_list_drush_help($section) {
  switch ($section) {
    case 'drush:sol-plugin':
      return dt('Download and install the SOL plugin from http://pbauerochse.github.io/searchable-option-list, default location is sites/all/libraries.');
  }
}

/**
 * Command to download the SOL plugin.
 */
function drush_searchable_option_list_plugin() {
  $args = func_get_args();
  if (!empty($args[0])) {
    $path = $args[0];
  }
  else {
    $path = 'sites/all/libraries';
  }

  // Create the path if it does not exist.
  if (!is_dir($path)) {
    drush_op('mkdir', $path);
    drush_log(dt('Directory @path was created', array('@path' => $path)), 'notice');
  }

  // Set the directory to the download location.
  $olddir = getcwd();
  chdir($path);

  // Download the zip archive.
  if ($filepath = drush_download_file(SOL_DOWNLOAD_URI)) {
    $filename = basename($filepath);
    $dirname_clean = 'searchable-option-list';
    $dirname_raw = 'searchable-option-list-master';

    // Remove any existing SOL plugin directory.
    if (is_dir($dirname_raw) || is_dir($dirname_clean)) {
      drush_delete_dir($dirname_raw, TRUE);
      drush_delete_dir($dirname_clean, TRUE);
      drush_log(dt('A existing SOL plugin was deleted from @path', array('@path' => $path)), 'notice');
    }

    // Decompress the zip archive.
    drush_tarball_extract($filename, '.');

    // Change the directory name to "searchable-option-list" if needed.
    if ($dirname_raw != $dirname_clean) {
      drush_move_dir($dirname_raw, $dirname_clean, TRUE);
    }
  }

  if (is_dir($dirname_clean)) {
    drush_log(dt('SOL plugin has been installed in @path', array('@path' => $path)), 'success');
  }
  else {
    drush_log(dt('Drush was unable to install the SOL plugin to @path', array('@path' => $path)), 'error');
  }

  // Set working directory back to the previous working directory.
  chdir($olddir);
}
